package com.horsitway.currencyconverter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.horsitway.currencyconverter.model.Conversion;
import com.horsitway.currencyconverter.service.ConvertService;

@RestController
public class Controller {
	
	@Autowired
	ConvertService service;
	
	@RequestMapping("/plustwo/{number}")
	public int plusTwo(@PathVariable int number) {
		return number + 2;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/convert")
	public double convert(@RequestBody Conversion conv) {
		double value = service.convert(conv); 
		return value;
	}
}
