package com.horsitway.currencyconverter.model;

public class Conversion {
	private String oldCurrency;
	private String newCurrency;
	private double value;
	
	public String getOldCurrency() {
		return oldCurrency;
	}
	public void setOldCurrency(String oldCurrency) {
		this.oldCurrency = oldCurrency;
	}
	public String getNewCurrency() {
		return newCurrency;
	}
	public void setNewCurrency(String newCurrency) {
		this.newCurrency = newCurrency;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}
