package com.horsitway.currencyconverter.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.horsitway.currencyconverter.model.Conversion;
import com.horsitway.currencyconverter.model.Rate;

@Service
public class ConvertService implements ConvertServiceIntf{

	JSONObject tableA;
	JSONObject tableB;
	
	Date downloadDate;
	ConvertService () {
		tableA = getRate("A");
		tableB = getRate("B");
		downloadDate = new Date();
	}
	
	@Override
	public JSONObject getRate(String table) {
		JSONObject json = new JSONObject();
		try {
			URL url = new URL("http://api.nbp.pl/api/exchangerates/tables/" + table + "/");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			//int responseCode = con.getResponseCode();
			//System.out.println("Code: " + responseCode);
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			//System.out.println(response.toString().substring(1, response.toString().length() - 1));
			json = new JSONObject(response.toString().substring(1, response.toString().length() - 1));
			
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}
		return json;
	}
	
	@Override
	public double convert(Conversion conv) {
		if(downloadDate.getDay() != new Date().getDay()) {
			tableA = getRate("A");
			tableB = getRate("B");
			downloadDate = new Date();
		}
		
		double originMid; //Assumption: Conversion is from PLN. It'll be verified in a moment.
		double targetMid;
		if (!conv.getNewCurrency().equals("PLN")) {
			targetMid = findMid(tableA, conv.getNewCurrency());
			if(targetMid == -1) targetMid = findMid(tableB, conv.getNewCurrency());
		}
		else {
			targetMid = 1;
		}
		
		if(!conv.getOldCurrency().equals("PLN")) { 				//Verify if conversion is from PLN
			originMid = findMid(tableA, conv.getOldCurrency()); //If not, find appropriate exchange rate
			if(originMid == -1) originMid = findMid(tableB, conv.getOldCurrency());
		}
		else {
			originMid = 1;
		}
		if (originMid == -1) return -2;
		else if (targetMid == -1) return -3;
		else return (conv.getValue() * originMid) / targetMid;
		
	}
	
	public double findMid(JSONObject table, String currency) {
		double mid = -1; //Currency with given code not found
		
		for (int i = 0; i < table.getJSONArray("rates").length() ; i++) {
			if(table.getJSONArray("rates").getJSONObject(i).getString("code").equals(currency)) {
				mid = table.getJSONArray("rates").getJSONObject(i).getDouble("mid");
				System.out.println(currency + " found!");
			}
		}
		return mid;
	}
}
