package com.horsitway.currencyconverter.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import org.json.JSONObject;

import com.horsitway.currencyconverter.model.Conversion;

public interface ConvertServiceIntf {
	
	public JSONObject getRate(String table);
	public double convert(Conversion conv);
}
